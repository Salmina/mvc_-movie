﻿using Microsoft.EntityFrameworkCore;
using Semestr3Pr.Models;

namespace Semestr3Pr.Data
{
    public class MvcMovieContext : DbContext
    {
        public MvcMovieContext(DbContextOptions<MvcMovieContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movie { get; set; }
    }
}
